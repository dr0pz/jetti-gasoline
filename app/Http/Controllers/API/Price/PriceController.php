<?php

namespace App\Http\Controllers\API\Price;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB\Product\Product;
use App\DB\Price\Price;

class PriceController extends Controller
{
    /**
     * App\DB\Product\Product
     * 
     * @var $product
     */
    protected $product;

    /**
     * App\DB\Price
     * 
     * @var $price
     */
    protected $price;

    /**
     * Dependency Injection
     *
     * @todo - Inject Middleware
     * @param Product $product
     * @param Price $price
     */
    public function __construct(Product $product, Price $price)
    {
        $this->product = $product;

        $this->price = $price;
    }

    /**
     * Show the prices of the given product id.
     *
     * @param type $id
     */
    public function index($id)
    {
        $product = $this->product->findOrFail($id);

        /* Get the form */
        $html = view('api.inventory.products.prices.index', compact('product'))->render();

        /* Render html from view. */
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Show the form for creating a new Price.
     *
     * @return Response
     */
    public function create(Request $request, $id)
    {

        $product = $this->product;

        /* Get the form */
        $html = view('api.inventory.products.prices.create', compact('product'))->render();

        // Render html from view.
        return response()->json(['success' => true, 'html' => $html]);
    }
}