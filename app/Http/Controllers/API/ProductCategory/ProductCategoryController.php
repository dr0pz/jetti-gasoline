<?php

namespace App\Http\Controllers\API\ProductCategory;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB\ProductCategory\ProductCategory;

class ProductCategoryController extends Controller
{
    /**
     * App\DB\ProductCategory\ProductCategory
     *
     * @var $productCategory
     */
    protected $productCategory;

    /**
     * Dependency Injection
     * 
     * @todo - Inject Middleware
     * @param ProductCategory $productCategory
     */
    public function __construct(ProductCategory $productCategory)
    {
        $this->productCategory = $productCategory;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        /* Get the form */
        $html = view('api.inventory.product-categories.create')->render();

        // Render html from view.
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        /**
         * Validate the Category Name
         */
        $this->validate($request, [
            'category_name' => 'required'
            ]
        );

        $productCategory = $this->productCategory->create($request->all());

        /* Generate the Row for our datatable */
        $row = $this->generateRow($productCategory);

        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @todo - Route model binding.
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $productCategory = $this->productCategory->findOrFail($id);

        /* Get the form */
        $html = view('api.inventory.product-categories.edit', compact('productCategory'))->render();

        /* Render html from view. */
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @todo - Route model binding.
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        /* Find the Model */
        $productCategory = $this->productCategory->findOrFail($id);

        /**
         * Validate the Category Name
         */
        $this->validate($request, [
            'category_name' => 'required'
            ]
        );

        /* Update the Resource */
        $productCategory->update($request->all());

        /* Generate the Row */
        $row = $this->generateRow($productCategory);

        /* Respond */
        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $productCategory = $this->productCategory->findOrFail($id);

        $productCategory->delete();

        return response()->json(['success' => true]);
    }

    /**
     * Generate the Row (To be used on our DataTable).
     *
     * @param ProductCategory $productCategory
     * @param type $data
     */
    protected function generateRow(ProductCategory $productCategory)
    {
        /* Generate the buttons */
        $actions = view('inventory.product-categories.partials.actions', compact('productCategory'))->render();

        $row = [
            'id' => $productCategory->id,
            'category_name' => $productCategory->category_name,
            'category_description' => $productCategory->category_description,
            'actions' => $actions
        ];

        return array_flatten($row);
    }
}