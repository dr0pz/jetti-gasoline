<?php

namespace App\Http\Controllers\API\Product;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\ProductRequest;
use App\Http\Controllers\Controller;
use App\DB\Product\Product;
use App\DB\Price\Price;
use App\DB\ProductCategory\ProductCategory;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    /**
     * App\DB\Product\Product
     * 
     * @var type 
     */
    protected $product;

    /**
     * App\DB\Price\Price
     * 
     * @var $price
     */
    protected $price;

    /**
     * App\DB\ProductCategory\ProductCategory
     * 
     * @var type
     */
    protected $productCategory;

    /**
     * Dependency Injection
     *
     * @todo - Inject Middleware
     * @param Product $product
     * @param Price $price
     * @param ProductCategory $productCategory 
     */
    public function __construct(Product $product, Price $price, ProductCategory $productCategory)
    {
        $this->product = $product;

        $this->productCategory = $productCategory;

        $this->price = $price;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        $product = $this->product;

        /* Generate List for selecting categories */
        $categories = withEmpty($this->productCategory->lists('category_name', 'id')->all(),
            '--Select Category--');

        /* Get the form */
        $html = view('api.inventory.products.create', compact('categories', 'product'))->render();

        // Render html from view.
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(ProductRequest $request)
    {

        /* Insert the new data. */
        $product = Auth::user()->branch->products()->create($request->all());

        /* Generate the Row for our datatable */
        $row = $this->generateRow($product);


        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @todo - Route model binding.
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {

        $product = $this->product->findOrFail($id);

        /* Generate List for selecting categories */
        $categories = withEmpty($this->productCategory->lists('category_name', 'id')->all(),
            '--Select Category--');

        /* Get the form */
        $html = view('api.inventory.products.edit', compact('product', 'categories'))->render();

        /* Render html from view. */
        return response()->json(['success' => true, 'html' => $html]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @todo - Route model binding.
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(ProductRequest $request, $id)
    {
        /* Find the Model */
        $product = $this->product->findOrFail($id);

        /* Update the Resource */
        $product->update($request->all());

        /* Generate the Row */
        $row = $this->generateRow($product);

        /* Respond */
        return response()->json(['success' => true, 'row' => $row]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);

        $product->delete();

        return response()->json(['success' => true]);
    }

   

    /**
     * Generate the Row (To be used on our DataTable).
     *
     * @param ProductCategory $product
     * @param type $data
     */
    protected function generateRow(Product $product)
    {

        /* Generate the buttons */
        $actions = view('inventory.products.partials.actions', compact('product'))->render();

        /* Get the Price */
        $price = (count($product->prices)) ? $product->prices()->latest()->amount : '-No Price Set-';

        $row = [
            'id' => $product->id,
            'name' => $product->product_name,
            'description' => $product->product_description,
            'price' => $price,
            'quantity' => $product->product_quantity,
            'category' => $product->category->category_name,
            'buttons' => $actions
        ];

        return array_flatten($row);
    }
}