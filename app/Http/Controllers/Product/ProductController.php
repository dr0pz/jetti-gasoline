<?php

namespace App\Http\Controllers\Product;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DB\Product\Product;
use Illuminate\Support\Facades\Auth;
use App\DB\Branch\Branch;

class ProductController extends Controller
{
    /* @var Product */
    protected $product;

    /**
     * Dependency Injection
     *
     * @todo - Inject Middleware
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        /**
         * Get all the Branch products that's currently handled by the
         * authenticated user
         */
        $products = Auth::user()->branch->products()->with(['prices', 'category'])->get();

        return view('inventory.products.index', compact('products'));
    }
}