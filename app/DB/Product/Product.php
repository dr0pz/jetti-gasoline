<?php

namespace App\DB\Product;

use Illuminate\Database\Eloquent\Model;
use App\DB\Product\Traits\ProductRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{

    use ProductRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_name',
        'category_id',
        'branch_id',
        'product_quantity',
        'product_description'
    ];



    /**
     * Set the category_id to null if the value is empty
     * 
     * @param type $value
     */
    public function setCategoryIdAttribute($value)
    {
        $this->attributes['category_id'] = $value ? : null;
    }



}