<?php

namespace App\DB\ProductCategory;

use Illuminate\Database\Eloquent\Model;
use App\DB\ProductCategory\Traits\ProductCategoryRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductCategory extends Model
{

    use ProductCategoryRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'category_name',
        'category_description'
    ];

}