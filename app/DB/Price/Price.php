<?php

namespace App\DB\Price;

use Illuminate\Database\Eloquent\Model;
use App\DB\Price\Traits\PriceRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Price extends Model
{

    use PriceRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'prices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'amount',
        'effective_date',
        'remarks'
    ];


    
    public function scopeLatest($query)
    {
        return $query->orderBy('effective_datetime', 'desc')->first();
    }

}