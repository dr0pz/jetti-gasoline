<?php

namespace App\DB\Price\Traits;

trait PriceRelationshipsTrait
{

    /**
     * Get the Products associated with this Price
     *
     * @return belongsToMany
     */
    public function product()
    {
        return $this->belongsTo('App\DB\Product\Product', 'product_id');
    }
}