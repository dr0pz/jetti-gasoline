<?php

namespace App\DB\Pump;

use Illuminate\Database\Eloquent\Model;
use App\DB\Pump\Traits\PumpRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pump extends Model
{

    use PumpRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pumps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pump_number',
        'pump_description'
    ];

}