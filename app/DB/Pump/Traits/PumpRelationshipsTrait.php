<?php

namespace App\DB\Pump\Traits;

trait PumpRelationshipsTrait
{

    /**
     * The Products associated with this Pump
     * 
     * @return belongsToMany
     */
    public function products()
    {
        return $this->belongsToMany('App\DB\Product\Product')->withTimestamps();
    }

    /**
     * The Readings associated with this Pump
     * 
     * @return type
     */
    public function readings()
    {
        return $this->hasMany('App\DB\ReadingPump\ReadingPump', 'pump_id', 'id');
    }
}