<?php

namespace App\DB\Reading;

use Illuminate\Database\Eloquent\Model;
use App\DB\Reading\Traits\ReadingRelationshipsTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reading extends Model
{

    use ReadingRelationshipsTrait,
        SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'readings';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'product_id',
        'reading_datetime',
        'remarks'
    ];

}