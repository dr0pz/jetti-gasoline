<?php

namespace App\DB\Reading\Traits;

trait ReadingRelationshipsTrait
{

    /**
     * The Product associated with this Reading
     * 
     * @return belongsTo
     */
    public function product()
    {
        return $this->belongsTo('App\DB\Product\Product', 'product_id');
    }

    /**
     * The Stick Reading associated with this Reading
     * 
     * @return type
     */
    public function stickReadings()
    {
        return $this->hasMany('App\DB\ReadingStick\ReadingStick', 'reading_id',
                'id');
    }

    /**
     * The Pump Reading associated with this Reading
     *
     * @return type
     */
    public function pumpReadings()
    {
        return $this->hasMany('App\DB\ReadingPump\ReadingPump', 'reading_id',
                'id');
    }
}