<?php

namespace App\DB\ReadingStick\Traits;

trait ReadingStickRelationshipsTrait
{

    /**
     * The Reading associated with this Stick Reading
     *
     * @return belongsTo
     */
    public function reading()
    {
        return $this->belongsTo('App\DB\Reading\Reading', 'reading_id');
    }
}