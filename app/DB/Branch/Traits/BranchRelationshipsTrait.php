<?php

namespace App\DB\Branch\Traits;

trait BranchRelationshipsTrait
{

    /**
     * Get the Users associated with this Branch
     *
     * @return hasMany
     */
    public function users()
    {
        return $this->hasMany('App\DB\User\User', 'branch_id', 'id');
    }

    /**
     * Get the Expenses associated with this Branch
     *
     * @return hasMany
     */
    public function expenses()
    {
        return $this->hasMany('App\DB\Expense\Expense', 'branch_id', 'id');
    }

    /**
     * Get the Products associated with this Branch
     *
     * @return hasMany
     */
    public function products()
    {
        return $this->hasMany('App\DB\Product\Product', 'branch_id', 'id');
    }
}