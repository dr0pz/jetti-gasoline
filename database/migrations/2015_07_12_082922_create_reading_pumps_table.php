<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadingPumpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reading_pumps', function (Blueprint $table) {
            $table->increments('id');
            
            $table->integer('reading_id')->unsigned();
            $table->foreign('reading_id')->references('id')->on('readings')
                ->onDelete('cascade');

            $table->integer('pump_id')->unsigned();
            $table->foreign('pump_id')->references('id')->on('pumps');
            
            $table->decimal('pump_opening');
            $table->decimal('pump_closing');
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reading_pumps');
    }
}
