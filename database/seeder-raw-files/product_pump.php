<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`product_pump`
$product_pump = array(
  array('id' => '1','product_id' => '1','pump_id' => '1','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','product_id' => '2','pump_id' => '2','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','product_id' => '3','pump_id' => '3','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '4','product_id' => '1','pump_id' => '2','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
