<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`readings`
$readings = array(
  array('id' => '1','product_id' => '1','reading_datetime' => '2015-07-15 00:00:00','remarks' => 'Sample Reading for Product 1','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','product_id' => '2','reading_datetime' => '2015-07-15 00:00:00','remarks' => 'Sample Reading for Product 2','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','product_id' => '3','reading_datetime' => '2015-07-16 00:00:00','remarks' => 'Sample Reading for Product 3','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
