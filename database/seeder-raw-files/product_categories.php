<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`product_categories`
$product_categories = array(
  array('id' => '1','category_name' => 'Category 1','category_description' => 'Category 1 Description','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','category_name' => 'Category 2','category_description' => 'Category 2 Description','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
