<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`reading_pumps`
$reading_pumps = array(
  array('id' => '1','reading_id' => '1','pump_id' => '1','pump_opening' => '20000.00','pump_closing' => '25000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','reading_id' => '2','pump_id' => '2','pump_opening' => '5000.00','pump_closing' => '7000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','reading_id' => '3','pump_id' => '3','pump_opening' => '7500.00','pump_closing' => '8000.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
