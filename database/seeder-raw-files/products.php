<?php
/**
 * Export to PHP Array plugin for PHPMyAdmin
 * @version 0.2b
 */

//
// Database `jetti-gasoline`
//

// `jetti-gasoline`.`products`
$products = array(
  array('id' => '1','category_id' => '1','product_name' => 'Product 1','product_description' => 'Product 1 Description','product_quantity' => '100.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '2','category_id' => '2','product_name' => 'Product 2','product_description' => 'Product 2 Description','product_quantity' => '50.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL),
  array('id' => '3','category_id' => '1','product_name' => 'Product 3','product_description' => 'Product 3 Desciption','product_quantity' => '35.00','created_at' => Carbon::now(),'updated_at' => Carbon::now(),'deleted_at' => NULL)
);
