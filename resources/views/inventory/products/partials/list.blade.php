@foreach ($products as $product)



<tr>
    <td>{{ $product->id }}</td>

    <td>{{ $product->product_name }}</td>
    <td>{{ $product->product_description }}</td>
    <td>
   
        {{ $product->prices->sortByDesc('effective_datetime')->first()->amount or '-No Price Set-' }}

    </td>
    <td>{{ $product->product_quantity }}</td>
    <td>
        {{ $product->category->category_name or "Uncategorized" }}
    </td>
    <td>
        @include('inventory.products.partials.actions', $product)
    </td>
</tr>



@endforeach