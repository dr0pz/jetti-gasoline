<div class='pull-right'>
    <div class="btn-group" role="group">

        <button
            type="button"
            class="btn btn-default btn-sm btn-products-edit"
            data-target="{{ action('API\Product\ProductController@edit', $product->id) }}"
            v-on="click: editProduct"
            v-attr="disabled: processingRequest"
        >
                <i class="fa fa-edit"></i> Edit
        </button>

        <button type="button"
           class="btn btn-default btn-sm btn-products-edit"
           data-target="{{ action('API\Price\PriceController@index', $product->id) }}"
           v-on="click: getPrices"
           v-attr="disabled: processingRequest"
           >
                <i class="fa fa-money"></i> Manage Price
        </button>

        <div class="btn-group">
            <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button">
                <span class="caret"></span>&nbsp;
            </button>

            <ul role="menu" class="dropdown-menu pull-right">
                <li>
                    <a
                        href="{{ action('API\Product\ProductController@destroy', $product->id) }}"
                        v-on="click: deleteProduct"
                    >
                        <i class="fa fa-trash-o"></i> Delete
                    </a>
                </li>
            </ul>

        </div>
    </div>
</div>