<div class="left_nav">
    <!--\\\\\\\left_nav start \\\\\\-->
    <div class="search_bar"> <i class="fa fa-search"></i>
        <input name="" type="text" class="search" placeholder="Search Dashboard..." />
    </div>
    <div class="left_nav_slidebar">
        <ul>


            <li class="{{ set_active('dashboard', 'left_nav_active theme_border') }}" > <a href="#"> <i class="fa fa-home"></i> DASHBOARD <span class="plus"><i class="fa fa-plus"></i></span></a>
                <ul>
                    <li> <a href="{{ url('dashboard') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Dashboard</b> </a> </li>
                </ul>
            </li>





            <li class="{{ set_active('readings', 'left_nav_active theme_border') }}">
                <a href="{{ url('readings') }}"> <i class="fa fa-dashboard "></i> Readings <span class="plus"></span></a>
            </li>



            <li> <a href="#"> <i class="fa fa-users"></i> Accounts <span class="plus"><i class="fa fa-plus"></i></span></a>
                <ul>
                    <li> <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Suppliers</b> </a> </li>
                </ul>
            </li>


            <li class="{{ set_active('inventory/*', 'left_nav_active theme_border') }}" > <a href="#"> <i class="fa fa-list-alt"></i> Inventory <span class="plus"><i class="fa fa-plus"></i></span></a>
                <ul class="{{ set_active('inventory/*', 'opened') }}" style="{{ set_active('inventory/*', 'display:block;') }}">
                    <li> 
                        <a href="{{ url('inventory/products') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Products</b> </a>
                    </li>
                    <li> 
                        <a href="{{ url('inventory/product-categories') }}"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Categories</b> </a>
                    </li>
                </ul>
            </li>
            <li> <a href="#"> <i class="fa fa-truck icon"></i> Orders <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Purchase Orders</b> </a> 
                    </li>
                </ul>
            </li>

            <li> <a href="#"> <i class="fa fa-truck icon"></i> Sales <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Purchase Orders</b> </a> 
                    </li>
                </ul>
            </li>

            <li> <a href="#"> <i class="fa fa-bar-chart-o icon"></i> Reports <span class="plus"><i class="fa fa-plus"></i></span> </a>
                <ul>
                    <li> 
                        <a href="#"> <span>&nbsp;</span> <i class="fa fa-circle"></i> <b>Sales Report</b> </a> 
                    </li>

                </ul>
            </li>

        </ul>
    </div>
</div>