<div class="modal-body">



    <div class="alert alert-danger alert-dismissible" v-show="errors" role="alert">
        <strong>Please correct the errors in the form.</strong>
        <ul class="">
            <li v-repeat="error: form.errors">@{{ error }}</li>
        </ul>
    </div>


            <div class='row'>

                <div class='col-sm-4'>
                    <dl>
                        <dt>Product Name:</dt>
                        <dd>{{ $product->product_name }}</dd>
                    </dl>
                </div>

                <div class="col-sm-4">
                    <dl>
                        <dt>Description:</dt>
                        <dd>{{ $product->product_description }}</dd>
                    </dl>
                </div>

                <div class='col-sm-4'>
                    <dl>
                        <dt>Category:</dt>
                        <dd>{{ $product->category->category_name or "Uncategorized" }}</dd>
                    </dl>
                </div>

            </div>


            <div class='row'>
                <div class='col-sm-12'>
                    <strong class="text-info">New Price</strong>
                    {!! Form::open(
                        [
                            'action' => 'API\Price\PriceController@store',
                            'id' => 'form-prices',
                            'v-on' => 'submit: addPrice'
                        ]
                    ) !!}

                    @include('api.inventory.products.prices.partials.form', ['submitBtnText' => 'Add'])

                    {!! Form::close() !!}
                </div>
            </div>
            

   

    <div class="block-web">
        <div class="header">
            <div class="actions"></div>
            <h3 class="content-header">History</h3>
        </div>
        <div class="porlets-content">

            <table class="table table-condensed disabled">
                <thead>
                    <tr>
                        <th>Amount</th>
                        <th>Date Effective</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody>
                    @include('api.inventory.products.prices.partials.list')
                </tbody>
            </table>

        </div><!--/porlets-content-->
    </div>








</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>



