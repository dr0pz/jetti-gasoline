<div class='pull-right'>
            <div class="btn-group" role="group">

                <a type="button" class="btn btn-default btn-sm btn-products-edit" href="{{ action('API\Product\ProductController@edit', $product->id) }}" ><i class="fa fa-edit"></i> Edit</a>

                <a type="button" class="btn btn-default btn-sm btn-products-edit" href="{{ action('API\Product\ProductController@edit', $product->id) }}" ><i class="fa fa-money"></i> Manage Price</a>

                <div class="btn-group">
                    <button data-toggle="dropdown" class="btn btn-default btn-sm dropdown-toggle" type="button">
                        <span class="caret"></span>&nbsp;
                    </button>

                    <ul role="menu" class="dropdown-menu pull-right">
                        <li><a href="{{ action('API\Product\ProductController@destroy', $product->id) }}" class="btn-products-delete"><i class="fa fa-trash-o"></i> Delete</a></li>
                    </ul>

                </div>
            </div>
        </div>