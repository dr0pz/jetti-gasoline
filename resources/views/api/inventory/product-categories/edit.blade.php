{!! Form::model(
    $productCategory,
    [
        'method' => 'PATCH',
        'action' => ['API\ProductCategory\ProductCategoryController@update', $productCategory->id],
        'id' => 'form-product-categories',
        'class' => 'form-horizontal',
        'v-on' => 'submit: updateCategory'
    ]
) !!}

    @include('api.inventory.product-categories.partials.form', ['submitBtnText' => 'Save'])

{!! Form::close() !!}

