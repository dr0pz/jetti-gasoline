
<div class="modal-body">

    <div class="alert alert-info alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>

        <span class="theme_color">*</span> Indicates required field.

    </div>


     <div class="alert alert-danger alert-dismissible" v-show="errors" role="alert">
        <strong>Please correct the errors in the form.</strong>
        <ul class="">
            <li v-repeat="error: form.errors">@{{ error }}</li>
        </ul>
    </div>


    <div class="form-group row has-feedback" v-class="has-error: form.errors.category_name">
        <label class="col-sm-3 control-label">Category Name <span class="theme_color">*</span></label>
        <div class="col-sm-9">
            {!! Form::text('category_name', null,
                [
                    'id' => 'category_name',
                    'class'=>'form-control',
                    'v-model' => 'inputs.category_name'
                ]
            ) !!}
            <span class="help-inline text-danger" v-show="form.errors.category_name">@{{ form.errors.category_name }}</span>
        </div>
    </div><!--/form-group-->


    <div class="form-group row has-feedback" v-class="has-error: form.errors.category_description">
        <label class="col-sm-3 control-label">Description</label>
        <div class="col-sm-9">
            {!! Form::text('category_description', null,
                [
                    'id' => 'category_description',
                    'class'=>'form-control',
                    'v-model' => 'inputs.category_description'
                ]
            ) !!}
            <span class="help-inline text-danger" v-show="form.errors.category_description">
                @{{ form.errors.category_description }}
            </span>
        </div>
    </div><!--/form-group-->

</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-primary" v-attr="disabled: processingRequest">{{ $submitBtnText }}</button>
</div>




