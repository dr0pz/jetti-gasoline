{!! Form::open(
    [
        'action' => 'API\ProductCategory\ProductCategoryController@store',
        'id' => 'form-product-categories',
        'class' => 'form-horizontal',
        'v-on' => 'submit: addCategory'
    ]
) !!}

    @include(
        'api.inventory.product-categories.partials.form',
        ['submitBtnText' => 'Add']
    )

{!! Form::close() !!}