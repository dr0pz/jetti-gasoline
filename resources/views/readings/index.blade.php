@extends('layouts.master')

@section('page-title', 'Dashboard | Jetti Gasoline')

@section('master-styles')
<link href="{{ asset('vendor/dataTables/DT_bootstrap.css') }}" rel="stylesheet">
@stop

@section('content')

<div class="pull-left breadcrumb_admin clear_both">
    <div class="pull-left page_title theme_color">
        <h1>Readings</h1>

        <!-- Subtitle if there's any -->
        <!--        <h2 class="">Subtitle here..</h2>-->

    </div>
    <div class="pull-right">
        <ol class="breadcrumb">
            <li class="active">Readings</li>
        </ol>
    </div>
</div>


<div class="container clear_both padding_fix">


    <!-- The Datepicker -->
    <div class="invoice_header">
        <div class="row">
            <div class="col-sm-4">
                <div class='row'>
                    <div class='col-md-6'>
                        <div class="input-group" id="dateselector-container" >
                            <input  type="text" title="Selected Date" name="dateselector" id="dateselector" class="form-control">
                            <span class="input-group-addon">
                                <span class="fa fa-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <p class="semi-bold help-block">Selected Date here</p>
                    </div>
                </div>

            </div>

            <div class="col-sm-8">
                <div class="btn-group pull-right">
                    <a class="btn btn-sm btn-success" id="reading-btn-create" > <i class="fa fa-plus"></i> New Reading </a>
                </div>

            </div>
        </div>
    </div>



    <!-- Datatable Start -->
    <div class="row">
        <div class="col-md-12">
            <div class="block-web">
                <div class="header">
                    <div class="actions"> 
                        <a class="minimize" href="#"><i class="fa fa-chevron-down"></i></a> 
                        <a class="refresh" href="#"><i class="fa fa-repeat"></i></a> 
                        <a class="close-down" href="#"><i class="fa fa-times"></i></a> 
                    </div>
                    <h3 class="content-header"><i class="">Date Selected</i> - Reading</h3>
                </div>
                <div class="porlets-content">
                    <div class="table-responsive">
                        <table  class="display table table-bordered table-striped" id="dynamic-table">
                            <thead>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th class="hidden-phone">Engine version</th>
                                    <th class="hidden-phone">CSS grade</th>
                                </tr>
                            </thead>
                            <tbody>

                                <tr class="gradeX">
                                    <td>Trident</td>
                                    <td>Internet
                                        Explorer 4.0</td>
                                    <td>Win 95+</td>
                                    <td class="center hidden-phone">4</td>
                                    <td class="center hidden-phone">X</td>
                                </tr>

                                
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Rendering engine</th>
                                    <th>Browser</th>
                                    <th>Platform(s)</th>
                                    <th class="hidden-phone">Engine version</th>
                                    <th class="hidden-phone">CSS grade</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div><!--/table-responsive-->
                </div><!--/porlets-content-->
            </div><!--/block-web--> 
        </div><!--/col-md-12--> 
    </div><!--/row-->


</div>



{!! Form::open(['url' => 'readings', 'id' => 'reading-form-create']) !!}


           
                    
{!! Form::close() !!}



@stop


@section('master-scripts')
<script src="{{ asset('vendor/dataTables/dataTables.js') }}"></script>
<script src="{{ asset('vendor/dataTables/DT_bootstrap.js') }}"></script>


<script>

$(document).on('click', '#reading-btn-create', function(){
    
    bootbox.dialog({
                title: "New Reading.",
                message: $('#reading-form-create'),
                buttons: {
                    success: {
                        label: "Save",
                        className: "btn-success",
                        callback: function () {
                            var name = $('#name').val();
                            var answer = $("input[name='awesomeness']:checked").val()
                            Example.show("Hello " + name + ". You've chosen <b>" + answer + "</b>");
                        }
                    }
                }
            }
        );
    
    
});


</script>

@stop