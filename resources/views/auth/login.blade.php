<!DOCTYPE html">
<html>
<head>
<meta charset="UTF-8">
<title>Login | Jetti Gasoline</title>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

<link href="{{ asset('vendor/jetti/css/font-awesome.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('vendor/jetti/css/admin.css') }}" rel="stylesheet">

</head>
<body class="light_theme  fixed_header left_nav_fixed">
    
<div class="wrapper">
  <!--\\\\\\\ wrapper Start \\\\\\-->

    <div class="login_page">
        <div class="login_content">
            <div class="panel-heading">
                <img src="{{ asset('images/xx.gif') }}" class="img img-responsive center-block">
            </div>

            
            
            <!-- Check for Login Error -->
            @if ($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoops!</strong>
                    There were some problems with your input.<br><br>
                    <ul>
                            @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                            @endforeach
                    </ul>
                </div>
            @endif

                            
            {!! Form::open(['method' => 'POST', 'url' => 'auth/login', 'id' => 'login-form', 'class' => 'form-horizontal']) !!}
            
                <div class="form-group">
                  <div class="col-sm-10">
                      <input type="text" placeholder="Username" id="username" name="username" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-10">
                    <input type="password" placeholder="Password" id="password" name="password" class="form-control">
                  </div>
                </div>

                
                <div class="form-group">
                  <div class=" col-sm-10">
                    <div class="checkbox checkbox_margin">
                      <label class="lable_margin">
                        <input type="checkbox" name="remember"><p class="pull-left"> Remember me</p></label>
                       
                        <button class="btn btn-default pull-right" type="submit">Sign in</button>
                    </div>
                  </div>
                </div>

            {!! Form::close() !!}
            
                
                

             
        </div>
    </div>

</div>
<!--\\\\\\\ wrapper end\\\\\\-->


<script src="{{ asset('vendor/jquery/jquery-1.11.3.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/common-script.js') }}"></script>
<script src="{{ asset('vendor/jetti/js/jquery.slimscroll.min.js') }}"></script>

</body>
</html>

